package com.myuw.homework252rnoar;
/** Homework 252
 *  5/1/2014
 *  Roger Noar
 *
 */


import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.app.ListFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.app.FragmentManager;

public class MainActivity extends ListActivity  {
    public static final String TAG = "MainActivity";

    private ActionBar mActionBar;
    private boolean mDualPane = true;
    private ListFragment mListFragment;
    private DetailsFragment mDetailsFragment;
    
    private Context mContext;
    
    private Cursor cursor;
    private TasksSQLiteOpenHelper mDatabaseHelper; 
    private SQLiteDatabase mReadableDB;
    private SQLiteDatabase mWritableDB;    
    int idMax; // last row ID of task table to make deleting most recent task easier
    int rowCnt = 0; // number of rows in task table
    
    private SimpleCursorAdapter TaskAdapter; // For our tasks list

/** SQLite Database Classes */
    private class PutTaskAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
        	// Get the task array from XML file
        	Resources res = getResources();
        	String[] myTaskArray = res.getStringArray(R.array.mytasks);
        	int len = myTaskArray.length;
        	int mTaskIndex = rowCnt;
        	mTaskIndex = mTaskIndex % len; //loop tasks if needed
        	// Add task from task array into database
            synchronized (mDatabaseHelper) {
                mWritableDB = mDatabaseHelper.getWritableDatabase();
                ContentValues cv = new ContentValues();
                cv.put(Tasks.Task.NAME, myTaskArray[mTaskIndex]);
                cv.put(Tasks.Task.DETAILS, rowCnt);
                mWritableDB.insert(Tasks.Task.TABLE_NAME, null, cv);
                mWritableDB.close();
            }
            return null;
        }
    }
    
    private class DeleteTaskAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            synchronized (mDatabaseHelper) {
                mWritableDB = mDatabaseHelper.getWritableDatabase();
                mWritableDB.delete(Tasks.Task.TABLE_NAME, "_ID="+ idMax , null);
                mWritableDB.close();
             }
            return null;
        }
    }    
    
    
    private class GetTasksAsyncTask extends AsyncTask<Void, Void, Void> {
        //Cursor cursor;
        @Override
        protected Void doInBackground(Void... params) {
            mReadableDB = mDatabaseHelper.getReadableDatabase();
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            qb.setTables(Tasks.Task.TABLE_NAME);
            cursor = qb.query(mReadableDB, Tasks.Task.PROJECTION, null, null, null, null, null);
            startManagingCursor(cursor);
            return null;
        }

        @Override
        protected void onPostExecute(Void response) {
        	// query finished, now do:
        	//
            //Get last row ID - to make deleting easier later
			if(cursor.moveToFirst()) {
                do{
                    idMax = cursor.getInt((0));
                }while(cursor.moveToNext());
			}
			rowCnt = cursor.getCount(); // get number of rows in table
            // Columns from the data to bind
            String[] from = new String[] { Tasks.Task.NAME, Tasks.Task.DETAILS };
            // The views to which the data will be bound
            int[] to = new int[] { android.R.id.text1, android.R.id.text2 };
            //SimpleCursorAdapter 
            TaskAdapter = new SimpleCursorAdapter(mContext, android.R.layout.simple_list_item_1, cursor, from, to);
            //setListAdapter(TaskAdapter);  // show the list - test without fragment
            // Show list using fragment
            FragmentManager fragmentManager = getFragmentManager();
            ListFragment listFragment = (ListFragment) fragmentManager.findFragmentById(R.id.listFragment);
            listFragment.getListView().setBackgroundColor(Color.parseColor("#442244")); //Grayish purple!
            listFragment.setListAdapter(TaskAdapter) ; //show task list fragment     
            
            mReadableDB.close();
            stopManagingCursor(cursor); // prevents "Unfortunately App has stopped" msg when exiting app
            //cursor.close(); //close the cursor - FYI can't close it here - too soon causes blank display

            super.onPostExecute(response);
          
        return;
        }
    } // End GetTasksAsyncTask    
    
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //main activity view

        mContext = this;        
        mDatabaseHelper = new TasksSQLiteOpenHelper(this);
        
        mListFragment = (ListFragment) getFragmentManager().findFragmentById(R.id.listFragment);        
        mDetailsFragment = new DetailsFragment();
        
        mActionBar = getActionBar();
        // mActionBar.setDisplayHomeAsUpEnabled(true); //Not needed here
        mActionBar.show();
        
    
    } //End onCreate
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }
    

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	
	    	case R.id.action1:
				//Toast.makeText(MainActivity.this, "Adding Task", Toast.LENGTH_SHORT) .show();
	              new PutTaskAsyncTask().execute(); // add task
	              new GetTasksAsyncTask().execute();  // show tasks
	   			break;
	    		
	    	case R.id.action2:
	    		if (rowCnt > 0) {
				//Toast.makeText(MainActivity.this, "Deleting Task", Toast.LENGTH_SHORT) .show();		
	                new DeleteTaskAsyncTask().execute(); //delete task
	                new GetTasksAsyncTask().execute();	// show tasks
	    		}
	             break;   
    	}
		return super.onOptionsItemSelected(item);
	}

    
	@Override
	protected void onResume() {
        /** Check to see if running on large display */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;        
        mDualPane = false; //default to single pane
        if (width > 720 && height >960) { //Use split pane if > 720x960 per HRD252
        	mDualPane = true; 
        }
        if (width > 900) {  // Also use split pane on my xhdpi phone if in landscape orientation
        	mDualPane = true; 
        }       
        if (mDualPane == false) { //Make the Details Fragment go away if not in Dual Pane mode
        findViewById(R.id.fragmentContainer).setVisibility(View.GONE);
        }
		new GetTasksAsyncTask().execute(); //show current tasks
		super.onResume();
	}	    
    
	
    /** Show details when list item clicked */
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        //Toast.makeText(this, ((TextView) v).getText(), Toast.LENGTH_SHORT).show();
        if (mDualPane) {
        	mDetailsFragment = new DetailsFragment();
        	String mTask = (String) ((TextView) v).getText() ; //task name
        	//Make a bundle to pass params to the fragment
    		Bundle args = new Bundle();
            args.putCharSequence("task", mTask ); //task name
            args.putInt("details", position); //row position    		
    		mDetailsFragment.setArguments(args);
    		
            // Show details as a fragment
            FragmentTransaction ft = getFragmentManager().beginTransaction();            
            ft.replace(R.id.fragmentContainer, mDetailsFragment);
            ft.commit();
        }
        
        else {  // not dual pane, so launch details activity instead of fragment
            Intent intent = new Intent();
            intent.setClass(this, DetailsActivity.class);
            intent.putExtra("task", ((TextView) v).getText() ); //task name
            intent.putExtra("details", position); //row position
            startActivity(intent);
        } 
    }   
        

}