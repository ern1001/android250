package com.myuw.homework252rnoar;


import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DetailsFragment extends Fragment {

    //private int mColor = 0xffff0000; // 
	private int mColor = 0xff000000; //black for background
	String mTask ;
	int mID;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment and set the color
        View view = inflater.inflate(R.layout.details_layout, container, false);
        // Populate the TextView with the argument bundle
        Bundle b = getArguments(); 
        mTask = b.getString("task");
        mID = b.getInt("details");
        mID = mID+1; // display task numbers starting at 1 instead of 0
          
  		//Show the task name and details
  		TextView detailsTitle = (TextView) view.findViewById(R.id.detailstitle);		
  		detailsTitle.setText(getString(R.string.detailstask));
        TextView detailsText = (TextView) view.findViewById(R.id.detailstext);		
  		detailsText.setText("This is Task #" + mID + ":  " + mTask );
        view.setBackgroundColor(0xff000000); //Black
        return view;
    }
    
    
    @Override
	public void onResume() {
        this.getView().setBackgroundColor(mColor);
		super.onResume();
	}



}
