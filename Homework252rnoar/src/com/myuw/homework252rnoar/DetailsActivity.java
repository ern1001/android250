package com.myuw.homework252rnoar;



import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class DetailsActivity extends Activity {
	private ActionBar mActionBar;
    public static final String TAG = "DetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.details_layout);

        //Show action bar
        mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true); // Up affordance
        mActionBar.show();
        
        Bundle b = getIntent().getExtras();
        String mTask = b.getString("task");
        int mID = b.getInt("details");
        mID = mID+1; // display task numbers starting at 1 instead of 0
		//Show the task name and details
		TextView detailsTitle = (TextView) findViewById(R.id.detailstitle);		
		detailsTitle.setText(getString(R.string.detailstask));
        TextView detailsText = (TextView) findViewById(R.id.detailstext);		
		detailsText.setText("This is Task #" + mID + ":  " + mTask );
        //detailsText.setText("Task Name: " + mTask + "\nTask Number: " + mID);
		View view = findViewById(R.id.detailsLayout);
        view.setBackgroundColor(0xff000000); //Black

	}	        
        


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected()");
        
        // Handle the up affordance
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);        
    }

    
}
