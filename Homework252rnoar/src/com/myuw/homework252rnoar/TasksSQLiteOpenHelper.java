package com.myuw.homework252rnoar;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class TasksSQLiteOpenHelper extends SQLiteOpenHelper {

    private final static String DB_NAME = Tasks.DATABASE_NAME;
    private final static int DB_VERSION = 1;

    private final static String TABLE_NAME = Tasks.Task.TABLE_NAME;
    private final static String TABLE_ROW_ID = Tasks.Task.ID;
    private final static String TABLE_ROW_NAME = Tasks.Task.NAME;
    private final static String TABLE_ROW_DETAILS = Tasks.Task.DETAILS;

    public TasksSQLiteOpenHelper(Context context) {

        super(context, DB_NAME, null, DB_VERSION);
    }

        
    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTableQueryString = 
                        "CREATE TABLE " + 
                        TABLE_NAME + " (" + 
                        TABLE_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + 
                        TABLE_ROW_NAME + " TEXT, " + 
                        TABLE_ROW_DETAILS + " TEXT" + ");";
        
        db.execSQL(createTableQueryString);
        ContentValues cv = new ContentValues();        
        db.insert(TABLE_NAME, null, cv);
        cv.clear();

    }

    @Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		super.onDowngrade(db, oldVersion, newVersion);
	}


	@Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

}
