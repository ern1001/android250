package com.myuw.homework251rnoar;
/** UW Homework 251
 *  4/18/2014
 *  R. Noar
 *
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Hide Action Bar
		android.app.ActionBar actionBar = getActionBar();
		actionBar.hide();
		
		// Sign In button
		Button signIn; 
		signIn = (Button) findViewById(R.id.signIn);
		signIn.setOnClickListener(this); //add listener for button clicks

	} // End onCreate	
	
	
	@Override
	protected void onResume() {
		super.onResume();
	}	
	
	// Listen for button clicks
	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.signIn) { // Verify the signIn button is pressed
			Log.v("Tag", "Sign In button clicked");
			
			//Get the email and password strings from the editText fields
			EditText f_email = (EditText) findViewById(R.id.f_email);
			String aEmail = f_email.getText().toString();
			
			EditText f_password = (EditText) findViewById(R.id.f_password);
			String aPassword = f_password.getText().toString();
		
			// Check for valid email & password strings	
			if (isValidEmail(aEmail) && aPassword.length() >0 ){
				launchSuccess(aEmail, aPassword); //valid so launch the success activity
			}
			else {  // not valid so show toast
				Toast.makeText(MainActivity.this,
						getString(R.string.invalidmsg),
						Toast.LENGTH_LONG) .show();
			}
		}
	} //End onClick listener

	// Check to see if email address is in a valid email format
	public final static boolean isValidEmail(String t) {
	    if (t == null) {
	        return false;
	    } else {
	        return android.util.Patterns.EMAIL_ADDRESS.matcher(t).matches();
	    }
	}
	
	// Launch the login success activity
	private void launchSuccess(String mEmail, String mPassword  ) {
		Intent intent = new Intent(this, LoginSuccessActivity.class);
		intent.putExtra("email", mEmail); // Use a bundle to pass these strings
		intent.putExtra("password", mPassword); // ""   ""
		startActivity(intent);
	}
	
	
} // End of Main Activity
