package com.myuw.homework251rnoar;
/** UW Homework 251
 *  4/18/2014
 *  R. Noar
 *
 */
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

	public class LoginSuccessActivity extends Activity  {

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_loginsuccess);
		
			//Hide Action Bar
			android.app.ActionBar actionBar = getActionBar();
			actionBar.hide(); 
			
			// Get email and password strings from bundle
			Bundle b = getIntent().getExtras();
			String email = b.getString("email");
			String password = b.getString("password");
			Log.v("Tag", "SuccessActivity "+ email + " "+ password);			
			
			// Build string to display
			StringBuilder sb = new StringBuilder();
			sb.append(getString(R.string.success));
			sb.append("\nEmail: " + email);
			sb.append("\nPassword: " + password);			

			//Show the email and password
			TextView successText = (TextView) findViewById(R.id.successtext);		
			successText.setText(sb.toString());
			}			
		
		@Override
		protected void onResume() {
			super.onResume();
		}		


	}
