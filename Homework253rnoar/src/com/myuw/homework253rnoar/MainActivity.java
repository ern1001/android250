package com.myuw.homework253rnoar;

/** Homework 253 
		5/20/2014
		R.Noar
		When button is pressed, start a background service to generated a beep
		every 5 seconds.
		Set button state according to status of background service running.
		Send notification if Main Activity is not visible.
		On click of notification, return to Main Activity and cancel notification.	
*/		

import java.util.Calendar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.preference.PreferenceManager;

public class MainActivity extends Activity implements OnClickListener{
	private static final String TAG = "HW253";
	public SharedPreferences sharedPreferences; //To keep track of service state even if app is killed

    private int mServiceState; // 1 if MyService is running, 0 otherwise
    public Button mButton; // start/stop button
    public Context mContext = this; // convenience
    public static boolean mMainVisible = true; // true if the main activity screen is visible
    
    Intent mIntent ; // Intent used to start/stop MyService 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        int threadID = android.os.Process.myTid();
        Log.d(TAG, "onCreate() on ThreadID: " + threadID);
        
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//Get shared preferences
	    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);   		
        
        mButton=(Button)findViewById(R.id.startButton);
        mButton.setOnClickListener(this);        
        
        mIntent = new Intent(mContext, MyService.class); // Intent for MyService class
        
	}//End onCreate
	

	   @Override
	    public void onClick(View v) {
	        switch(v.getId()) {
	            case R.id.startButton:
	                // Check to see if the service is already running
	                if (mServiceState == 0) {
		                Log.d(TAG, "startService()");
		                mServiceState = 1;
		                startService(mIntent); // Start the background service
	                }
	                
	                else {
		                Log.d(TAG, "stopService()");
		                mServiceState = 0;
		                stopService(mIntent); // Stop the background service
	                }
                
	                break; 
	        }
	        saveButtonState(); // Save button state
	        restoreButtonState(); // Update button text
	        
	    }// End onClick

		@Override
		protected void onResume() {		
            Log.d(TAG, "onResume()");
			restoreButtonState(); // Check if service is already running & set button text accordingly
			mMainVisible = true; //We are here, so MainActivity must be visible
			
			// Cancel out any prior issued notifications
		  	int mNotificationId = 314159; //to keep track of this notification
		  	NotificationManager mNotifyMgr = 
		  	        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);			
 			mNotifyMgr.cancel(mNotificationId);
 			
			super.onResume();
		}	

		@Override
		protected void onPause() {	
            Log.d(TAG, "onPause()");			
			mMainVisible = false; //Main Activity is not visible
			super.onPause();
		}			

		@Override
		protected void onStop() {	
            Log.d(TAG, "onStop()");			
			mMainVisible = false; //Main Activity is not visible
			super.onStop();
		}		
		
		
		/** Save running state of myService, used for determining button text state */
		public void saveButtonState() {
			Editor editor = sharedPreferences.edit();
			editor.putInt("mServiceState", mServiceState);	
			editor.commit();
		}

		/**Restore button text state based upon checking stored shared preference 	*/	
		public void restoreButtonState() {
            mServiceState = sharedPreferences.getInt("mServiceState", 0);
	 		if (mServiceState == 1) {
	 			mButton.setText("Stop" );   //Change button text
	 		}
	 		else {
	 			mButton.setText("Start" );   //Change button text
	 		}	
		}

		

}
