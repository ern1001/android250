package com.myuw.homework253rnoar;
/** Homework 253 MyService
5/20/2014
R.Noar
Service that runs in the background that
generates repeating beeps and sends a notification
if the MainActiviy is not visible.
*/	

import java.io.IOException;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

 public class MyService extends Service {
   private static final String TAG = "HW253";
   MyAsyncTask mTask; // For AsyncTask below
   
  @Override
  public void onCreate() {
	  Log.d(TAG, "MyService onCreate");  
      //Toast.makeText(this, "Service Created", Toast.LENGTH_SHORT).show();       		  
	  super.onCreate();
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
	 Log.d(TAG, "MyService onStart");
     //Toast.makeText(this, "Beeping Service Started", Toast.LENGTH_SHORT).show();   
     
     /** Make a repeating Beep
      Run it as an AsyncTask thread so not impacting UI thread
      **/
	 mTask = new MyAsyncTask();
	 mTask.execute(); //Start AsyncTask

	 return Service.START_REDELIVER_INTENT; //Restarts the service with same intent if the OS kills it off sometime...
    }// End onStart

  	  @Override
  	  public IBinder onBind(Intent intent) {
  	      //Toast.makeText(this, "Service onBind", Toast.LENGTH_SHORT).show();  		  
  		  Log.d(TAG, "MyService onBind");    		  
  	  //TODO for communication return IBinder implementation
  	    return null;
  	  }
  	  
  	   @Override
  	   public void onDestroy() {
   	      //Toast.makeText(this, "MyService onDestroy", Toast.LENGTH_SHORT).show();  		  
   		  Log.d(TAG, "MyService onDestroy");  		   
  	      mTask.cancel(true); // cancel the AsyncTask when service is destroyed
  	      super.onDestroy();
  	   }   	
  	   
  	   private class MyAsyncTask extends AsyncTask<Void, Void, Void> {
			@Override
			protected Void doInBackground (Void...  params ) {
				while (isCancelled() == false) { // Do forever until cancelled
					  
					 final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_ALARM, 100); // could also use STREAM_NOTIFICATION, but not as loud
				     try {
				    	 tg.startTone(ToneGenerator.TONE_PROP_BEEP); // standard android general beep
					     }  catch (Exception e)  {	
					    	e.printStackTrace(); 
					    	 Log.d(TAG, "ToneGenerator - Exception: " + e);
					     } finally  {
					    	 for (int i = 0; i <10; i++ ) { //Do this loop 10 times so we can check for cancelled more often
					    		 SystemClock.sleep(500); // wait 500msec * 10 loops = 5 seconds total 	 
					    	 	if (isCancelled() == true) { // exit if the service is cancelled
					    		 	break;
					    	 	}//End if
					    	 }//End for
					    	 tg.release(); // ! Important - free up audio track resources or it faults after 32 times
					     }
					    mSendNotify(); // Send notification if needed
					 } // End while
				return null;
			}//End doInBackground
  	   }//End AsyncTask  	 
  	   
  	   
  	   /** Method sends a notification if the MainActivity screen is not visible */
  	   public void mSendNotify() {
       /**Only send notification if MainActivity is NOT visible */
       if ( MainActivity.mMainVisible == false) {
  	  	 /** Create intent so user can click on notification & return to main activity */
  	  	 Intent notifIntent = new Intent(this, MainActivity.class);
  	  	 /** Notification pending intent */
  	  	 PendingIntent resultPendingIntent =
  	  	    PendingIntent.getActivity(this, 0, notifIntent, PendingIntent.FLAG_UPDATE_CURRENT );    	 
      	 //Build the notification
  	     NotificationCompat.Builder mBuilder =
  			    new NotificationCompat.Builder(this)
  			    .setSmallIcon(R.drawable.bad_andy)
  			    .setContentTitle(getString(R.string.notifTitle))
  			    .setContentText(getString(R.string.notifText))
  			    .setContentIntent(resultPendingIntent);	//
  		    Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.bad_andy);
       		mBuilder.setLargeIcon(bm); //Large icon used for ticker display
  	  	  
  	     Notification notification = mBuilder.build(); // build the notification
  	     notification.tickerText = getString(R.string.notifTitle); // notification bar ticker text
  	  	 notification.flags |= Notification.FLAG_AUTO_CANCEL; // cancel notification on click
      	 // Sets an ID for the notification
  	  	 int mNotificationId = 314159;
  	  	 // Gets an instance of the NotificationManager service
  	  	 NotificationManager mNotifyMgr = 
  	  	         (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
  	  	 // Send the notification
  	  	 mNotifyMgr.notify(mNotificationId, notification);  
       } //End if	   
  	   } //End  mSendNotify
  	   
  
  	  
 }  //End MyService   	
	

